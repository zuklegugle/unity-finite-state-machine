using System.Linq.Expressions;
using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Reflection;

namespace FSM.Editor
{
    public class FSMEditor : EditorWindow
    {
        private StateMachine editedAsset; 
        private List<Type> exposedStateClasses = new List<Type>();
        private List<StateData> stateList = new List<StateData>();
        // creating the window
        [MenuItem("Window/FSM/Editor")]
        public static FSMEditor OpenFSMEditor()
        {
            return EditorWindow.GetWindow<FSMEditor>("State Machine");
        }
        // opening an FSM asset
        [OnOpenAssetAttribute(1)]
        public static bool OnOpenFSMAsset(int instanceID, int line)
        {
            StateMachine asset = EditorUtility.InstanceIDToObject(instanceID) as StateMachine;
            if (asset != null)
            {
                FSMEditor editorWindow = OpenFSMEditor();
                editorWindow.editedAsset = asset;
                Debug.Log("FSM Asset loaded");
                return true;
            }
            return false; // we did not handle the open
        }
        // Callbacks
        private void OnEnable()
        {
            var types = System.AppDomain.CurrentDomain.GetAllDerivedTypes(typeof(FSMBaseState));
            foreach (Type type in types)
            {
                FSMStateAttribute attribute = type.GetCustomAttribute<FSMStateAttribute>();
                stateList.Add( new StateData(type, attribute.DisplayName));
                //var n = attribute.DisplayName;
                //var names = n.Split('/');
               // Debug.Log("Loaded state [" + names[0]+","+names[1] + "]");
                //exposedStateClasses.Add(type);
                //Debug.Log("added type -" + type.Name); 
            }
        }

        private void OnGUI()
        {
            if (editedAsset != null)
            {
                // HandleEvents(Event.current);
                // EditorGUILayout.LabelField("exposed states", EditorStyles.boldLabel);
                // foreach (Type type in exposedStateClasses)
                // {
                // EditorGUILayout.LabelField($"{type.Name} - {type.ReflectedType}");
                // }

                HandleEvents(Event.current);
                PaintNodes();
                Repaint();
            }
        }

        private void HandleEvents(Event e)
        {
            foreach (EditorStateNode stateNode in editedAsset.stateNodes)
            {
                stateNode.HandleEvents(e);
            }
            switch(e.type)
            {
                case EventType.MouseDown:
                if (e.button == 1)
                {
                    OpenContextMenu(e.mousePosition);
                    //Debug.Log("opened menu");
                }
                break;
            }
        }
        // Methods
        private void PaintNodes()
        {
           foreach(EditorStateNode stateNode in editedAsset.stateNodes)
           {
               stateNode.Paint();
           }
        }
        private void OpenContextMenu(Vector2 mousePosition)
        {
            GenericMenu genericMenu = new GenericMenu();
            foreach (StateData state in stateList)
            {
                var attStrings = state.name.Split('/');
                string attName = attStrings[attStrings.Length-1];
               genericMenu.AddItem(new GUIContent(state.name), false, () => OnAddToMenu(mousePosition, attName));
            }
            genericMenu.ShowAsContext();
        }

        void AddStateToMenuSeparated(GenericMenu menu, string name)
        {       
           // menu.AddItem(new GUIContent(name), false, () => OnAddToMenu());
        }
        void OnAddToMenu(Vector2 mousePosition, string name)
        {
            EditorStateNode node = new EditorStateNode(mousePosition, 200, 30, name);
            editedAsset.stateNodes.Add(node);
        }

        public static List<string> SplitString(string str)
        {
            List<string> result = new List<string>();
            if (str.Length != 0)
            {
                char[] chars = str.ToCharArray();
                int slashPosition = 0;
                string currentWord = "";
                int index = 0;
                foreach(char symbol in chars)
                {
                    if (!symbol.Equals("/"))
                    {
                        if(slashPosition == 0)
                        {
                            currentWord += symbol;
                        }
                        else
                        {
                            if(index > slashPosition)
                            {
                                currentWord += symbol;
                            }
                        }
                    }
                    else
                    {
                        slashPosition = index;
                        result.Add(currentWord);
                        currentWord = "";
                    }
                    index ++;
                }
                return result;
            }
            else
            {
                return null;
            }
        }
    }
}