using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSM
{
    [System.Serializable]
    public class StateData
    {
            public Type type;
            public string name;

            public StateData(Type t, string n)
            {
                type = t;
                name = n;
            }
    }
}