using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSM
{
    [System.Serializable]
    public class EditorStateNode
    {
        public Rect box;
        public string label;
        public StateData data;

        public EditorStateNode(Vector2 position, float width, float height, string text)
        {
            box = new Rect(position, new Vector2(width, height));
            label = text;
        }

        public void HandleEvents(Event e)
        {
            switch(e.type)
            {
                case EventType.MouseDrag:
                if (box.Contains(e.mousePosition))
                {
                    box.position += e.delta;
                }
                break;
            }
        }

        public void Paint()
        {
            GUI.Box(box, label);
        }
    }
}