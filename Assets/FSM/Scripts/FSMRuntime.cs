using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSM 
{
    public class FSMRuntime : MonoBehaviour
    {
        public StateMachine stateMachine;

        public FSMBaseState currentState;

        void Start()
        {

        }
        void Update()
        {
            if(currentState == null) return;
            //---------
        }
        public void SwitchState(FSMBaseState state)
        {
            if (state != null)
            {
                if (currentState != null)
                {
                    ExitState();
                    EnterState(state);
                }
                else
                {
                    EnterState(state);
                }
            }
        }
        private void EnterState(FSMBaseState state)
        {
            currentState = state;
            currentState.OnEnter();
        }
        private void ExitState()
        {
            currentState.OnExit();
            currentState.Reset();
            currentState = null;
        }
    }
}