using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSM
{
    [CreateAssetMenu(menuName="FSM/State Machine")]
    public class StateMachine : ScriptableObject
    {
        [SerializeField] public List<EditorStateNode> stateNodes = new List<EditorStateNode>();
    }
}