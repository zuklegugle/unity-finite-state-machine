using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSM
{
    public abstract class FSMBaseState
    {
        public FSMRuntime stateMachine;
        public bool InitState(FSMRuntime machine)
        {
            if (machine == null)
            {
                Debug.Log("State initialisation failed - no runtime specified");
                return false;
            }
            else
            {
                stateMachine = machine;
                return true;
            }
        }
        public virtual void OnEnter(){;}
        public virtual void OnRun(){;}
        public virtual void OnExit(){;}
        public virtual void Reset(){;}
    }
}
