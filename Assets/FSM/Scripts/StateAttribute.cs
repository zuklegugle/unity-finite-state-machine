using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSM 
{
    [AttributeUsage(AttributeTargets.Class)]
    public class FSMStateAttribute : Attribute
    {
        public string DisplayName;
        public FSMStateAttribute(string displayName)
        {
            DisplayName = displayName;
        }
    }
}
