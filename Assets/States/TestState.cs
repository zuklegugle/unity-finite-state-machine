using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;

[FSMState("Test/State")]
public class TestState : FSMBaseState
{
    public override void OnEnter()
    {
        Debug.Log("Entered test state!");
    }
}

[FSMState("Player/Idle")]
public class PlayerIdleState : FSMBaseState
{
    public override void OnEnter()
    {
        Debug.Log("Entered test state!");
    }
}

[FSMState("Player/Move")]
public class PlayerMoveState : FSMBaseState
{
    public override void OnEnter()
    {
        Debug.Log("Entered test state!");
    }
}